function Track(data) {
    this.data = data;
    this.name = data.name;
    this.lanes = data.lanes;
    this.pieces = data.pieces;

    this.preCalculate();
}

Track.prototype.nextPiece = function(i) {
    return this.pieces[(i + 1 < this.pieces.length) ? i + 1 : 0];
}

Track.prototype.shortestLane = function(i) {
    return this.pieces[i].shortestLane;
}

Track.prototype.preCalculate = function() {
    // Calculate bend lengths.
    this.pieces.forEach(function(p) {
        if (p.angle) {
            p.length = new Object();
            this.lanes.forEach(function(lane) {
                var laneRadius = p.radius;
                if (p.angle < 0) {
                    laneRadius -= lane.distanceFromCenter;
                } else {
                    laneRadius += lane.distanceFromCenter;
                }
                p.length[lane.index] = Math.abs(p.angle) * Math.PI * laneRadius / 180;
            });
        }
    }, this);

    // Calculate switch preferences.
    this.pieces.forEach(function(piece, index) {
        if (piece.switch) {
            var laneLengths = new Object();

            // Iterate until the next switch piece.
            var nextPiece = this.nextPiece(index);
            while (!nextPiece.switch) {
                this.lanes.forEach(function(lane) {
                    if (!laneLengths[lane.index]) {
                        laneLengths[lane.index] = 0;
                    }

                    if (typeof nextPiece.length === "number") {
                        // Straight piece.
                        laneLengths[lane.index] += nextPiece.length;
                    } else {
                        // Bend piece.
                        laneLengths[lane.index] += nextPiece.length[lane.index];
                    }
                });
                nextPiece = this.nextPiece(this.pieces.indexOf(nextPiece));
            }

            // Define the shortest lane.
            var allEqual = this.lanes.every(function(lane) {
                return laneLengths[lane.index] == laneLengths[this.lanes[0].index];
            }, this);
            if (!allEqual) {
                piece.shortestLane = this.lanes[0].index;
                this.lanes.forEach(function(lane) {
                    if (laneLengths[piece.shortestLane] < laneLengths[lane.index]) {
                        piece.shortestLane = lane.index;
                    }
                });
            }
        }
    }, this);

    // Initialize speed limits.
    this.pieces.forEach(function(piece) {
        if (piece.angle) {
            piece.speedLimit = 4;
        } else {
            piece.speedLimit = 10;
        }
    });
}

// Export the constructor.
module.exports = Track;
