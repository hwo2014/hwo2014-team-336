var net = require("net");
var Bot = require("./bot.js");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var responseSent = false;

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
    return send({
        msgType: "join",
        data: {
            name: botName,
            key: botKey
        }
    });
});

function send(json) {
    responseSent = true;
    client.write(JSON.stringify(json));
    return client.write('\n');
};
var bot = new Bot(send);

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
    responseSent = false;

    // Route messages to the bot by their msgType.
    if (typeof bot[data.msgType] === "function" && data.data !== undefined) {
        bot[data.msgType](data.data, data.gameTick);
    } else {
        console.log("No routing for msgType: " + data.msgType + ", data: " + JSON.stringify(data));
    }

    if (!responseSent) {
        // Send the default response, if nothing else was sent.
        bot.default(data);
    }
});

jsonStream.on('error', function() {
    return console.log("disconnected");
});
