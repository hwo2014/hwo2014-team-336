var Track = require("./track.js");
var Colors = require("./terminal-colors.js");

/** Takes a function for communicating to the outside world as the sole argument. */
function Bot(send) {
    this.send = send;
    this.color = "";
    this.track = null;
    this.running = false;

    this.previousThrottle = 0;
    this.previousCarData;
    this.previousGameTick;
    this.switchingLanes = false;
    this.maxSlipAngle = 0;
    this.crashingSlipAngle = 59;
    this.speed = 0;
}

Bot.prototype.gameInit = function(data) {
    if (this.track == null) {
        this.track = new Track(data.race.track);
    }
    console.log("Getting ready to race on", this.track.name);
    console.log(JSON.stringify(data));
}

Bot.prototype.join = function(data) {
    console.log('Joined');
}

Bot.prototype.gameStart = function(data) {
    console.log("Race started");
    this.running = true;
}

Bot.prototype.gameEnd = function(data) {
    console.log("\nRace ended");
    this.running = false;
}

Bot.prototype.yourCar = function(data) {
    this.color = data.color;
    console.log("I'm known as the", this.color, "car");
}

Bot.prototype.lapFinished = function(data) {
    console.log("Lap", data.lapTime.lap + 1, "finished in", data.lapTime.millis, "ms");
}

Bot.prototype.crash = function(data) {
    console.log(Colors.red + "Crashed!" + Colors.reset);
    this.running = false;
    this.crashingSlipAngle = this.maxSlipAngle;
}

Bot.prototype.spawn = function(data) {
    console.log(Colors.red + "Spawn!" + Colors.reset);
    this.throttle = 0;
    this.running = true;
}

Bot.prototype.carPositions = function(data, gameTick) {
    if (!this.running) {
        return;
    }

    var carData = this.getMyCar(data);
    this.updateStats(carData, gameTick);

    var currentPosition = carData.piecePosition.pieceIndex;
    var previousPosition = (this.previousCarData === undefined ? -1 : this.previousCarData.piecePosition.pieceIndex);
    if (previousPosition != currentPosition) {
        // Handle entering a new piece.
        this.onNewPiece(currentPosition, previousPosition, carData);
    }

    if (!this.switchingLanes) {
        // Handle throttle control.
        var thisPiece = this.track.pieces[carData.piecePosition.pieceIndex];
        var nextPiece = this.track.nextPiece(carData.piecePosition.pieceIndex);

        // Define the target speed by looking at the speed limit of this and next piece.
        var throttle = this.previousThrottle;
        var targetSpeed = Math.min(thisPiece.speedLimit, nextPiece.speedLimit);
        var speedDiff = targetSpeed - this.speed;

        throttle += speedDiff * 0.1;

        // Limit to 0-1 range.
        throttle = Math.min(1, Math.max(0, throttle));

        // console.log("tick: " + gameTick + ", throttle: " + throttle + ", slip angle: " + carData.angle + ", speed: " + this.speed + ", targetSpeed: " + targetSpeed + ", diff: " + speedDiff + ", maxSlip: " + this.maxSlipAngle);
        this.send({
            msgType: "throttle",
            data: throttle
        });
        this.previousThrottle = throttle;
    }

    this.switchingLanes = false;
    this.previousCarData = carData;
    this.previousGameTick = gameTick;
}

Bot.prototype.updateStats = function(carData, gameTick) {
    // Update speed.
    if (this.previousCarData !== undefined) {
        var previousPos = this.previousCarData.piecePosition;
        var currentPos = carData.piecePosition;

        if (previousPos.pieceIndex == currentPos.pieceIndex) {
            // We're still on a same piece and can update the speed.
            var movedDistance = currentPos.inPieceDistance - previousPos.inPieceDistance;
            var duration = gameTick - this.previousGameTick;
            this.speed = movedDistance / duration;
        }
    }

    // Update maximum slip angle.
    if (Math.abs(carData.angle) > this.maxSlipAngle) {
        this.maxSlipAngle = Math.abs(carData.angle);
    }
}

Bot.prototype.getMyCar = function(data) {
    return data.filter(function(i) { return i.id.color === this.color }.bind(this))[0];
}

Bot.prototype.onNewPiece = function(currentPosition, previousPosition, carData) {
    var lane = carData.piecePosition.lane.endLaneIndex;
    // console.log(Colors.green + "Entered piece #" + currentPosition + ": " + Colors.yellow + JSON.stringify(this.track.pieces[currentPosition]) + Colors.reset);

    // Determine if we need to switch lanes on the next piece.
    var shortestLane = this.track.nextPiece(currentPosition).shortestLane;
    if (typeof shortestLane !== "undefined" && lane != shortestLane) {
        // console.log(Colors.green + "Switching from lanes " + lane + " to " + shortestLane + " [" + (shortestLane > lane ? "Right" : "Left") + "]" + Colors.reset);

        this.send({
            msgType: "switchLane",
            data: (shortestLane > lane ? "Right" : "Left")
        });
        this.switchingLanes = true;
    }

    // Update speed limits according to maxSlipAngle
    if (previousPosition >= 0) {
        // Update the speed limit according to slip angle.
        this.track.pieces[previousPosition].speedLimit += (this.crashingSlipAngle / 3 - this.maxSlipAngle) * 0.05;
    }
    // Reset the slip angle.
    this.maxSlipAngle = 0;
}

Bot.prototype.default = function(data) {
    this.send({ msgType: "ping", data: { } });
}

// Export the constructor.
module.exports = Bot;
